# Overview #
This repo contains few semantic-check tests for Refapp using JUnit. It consists of a REST API test and a HtmlUnit test for verifying front end. This can be used as a reference for writing detailed SC for products. 

# How to build #

```
#!bash
# Build the tests
mvn clean install -DskipTests

# Build the Docker image
docker build -t docker.atlassian.io/refapp/refapp-sc .

# Running the tests locally
docker run --env SERVICE_URL=<<REFAPP_ENDPOINT>> docker.atlassian.io/refapp/refapp-sc

# Example
docker run --env SERVICE_URL=http://172.22.44.138:5990/refapp docker.atlassian.io/refapp/refapp-sc

# Run using Semantic Check Executor
Refer https://semantic-check-executor.internal.domain.dev.atlassian.io/api.html
```
