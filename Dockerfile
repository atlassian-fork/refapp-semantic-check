FROM java:8-jre-alpine 

RUN mkdir -p /refapp-lib
COPY ./target/alljars /refapp-lib
COPY ./target/refapp-semantic-check-*-tests.jar /refapp-lib

ENV SERVICE_URL="http://localhost:5990/refapp"

CMD java -cp $(for i in refapp-lib/*.jar ; do echo -n $i: ; done) org.junit.runner.JUnitCore com.atlassian.refapp.sc.RefappUISemanticCheckTest com.atlassian.refapp.sc.RefappAPISemanticCheckTest
